require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../installatron-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


    const username = "server1-12356"

	describe('guitransfer request', function() {



        it('as a regular user without ftp_username should be rejected', function(done) {


             mapi.post( 
                "/webhosting/guixfer",
                {
                       server: "server1",
                       webhosting_id: 12356,
                       label: "Some fancy webhosting package",
                       "something":"else",
                       vhosts: {
                           "http://some.website1.tld/": "/some/subdirectory1/pages",
                           "http://some.website2.tld/": "/some/subdirectory2/pages",
                       },
                       language: "hu",
                       email: "email@email.hu",
                }, 
             function(err, result, httpResponse){
                // console.log(err, result)
                 assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })


        })

        it('as a regular user with path should be ok', function(done) {

            app.sendRequestToInstallatron = function(payload) {
                assert.deepEqual(payload, {language: 'hu',
                  server: 'server1.monstermedia.hu',
                  mysql_host: 'server1.monstermedia.hu',
  websites:
   [ { id: 'server1-12356',
       label: 'Some fancy webhosting package',
       path: 'ftp://ftpuser@server1.monstermedia.hu',
       db_mode: 'manual',
       vhosts: { 'http://some.website1.tld/': '/some/subdirectory1/pages/',
  'http://some.website2.tld/': '/some/subdirectory2/pages/' },
  databases:[] } ],
  email: 'email@email.hu',
  key: 'api_key',
  cmd: 'guixfer' })

                return Promise.resolve({result: JSON.stringify({
                    session_id: "session_id_that_was_just_received"
                })})
            }

             mapi.post( 
                "/webhosting/guixfer",
                {
                       server: "server1",
                       webhosting_id: 12356,
                       ftp_username: "ftpuser",
                       label: "Some fancy webhosting package",
                       "something":"else",
                       vhosts: {
                           "http://some.website1.tld/": "/some/subdirectory1/pages",
                           "http://some.website2.tld/": "/some/subdirectory2/pages",
                       },
                       language: "hu",
                       email: "email@email.hu",
                }, 
             function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, {"url":"https://some.public.facing.tld/installatron/index.php?s=session_id_that_was_just_received"})
                done()

             })

        })


        it('as a regular user with database names', function(done) {

            app.sendRequestToInstallatron = function(payload) {

                assert.deepEqual(payload, {language: 'hu',
                  server: 'server1.monstermedia.hu',
                  mysql_host: 'server1.monstermedia.hu',
  websites:
   [ { id: 'server1-12356',
       label: 'Some fancy webhosting package',
       path: 'ftp://ftpuser@server1.monstermedia.hu',
       db_mode: 'manual',
       vhosts: { 'http://some.website1.tld/': '/some/subdirectory1/pages/',
  'http://some.website2.tld/': '/some/subdirectory2/pages/' },
databases: [
                         {
                            "type": "mysql",
                            "host": "server1.monstermedia.hu",
                            "name": "database_name1",
                            "user": "database_user1",
                         },
                         {
                            "type": "mysql",
                            "host": "server1.monstermedia.hu",
                            "name": "database_name2",
                            "user": "database_user2",
                         },                         
                       ]
   } ],
  email: 'email@email.hu',
  key: 'api_key',
  cmd: 'guixfer' })


                return Promise.resolve({result: JSON.stringify({
                    session_id: "session_id_that_was_just_received"
                })})
            }

             mapi.post( 
                "/webhosting/guixfer",
                {
                       server: "server1",
                       webhosting_id: 12356,
                       ftp_username: "ftpuser",
                       label: "Some fancy webhosting package",
                       "something":"else",
                       vhosts: {
                           "http://some.website1.tld/": "/some/subdirectory1/pages",
                           "http://some.website2.tld/": "/some/subdirectory2/pages",
                       },
                       databases: [
                         {
                            "type": "mysql",
                            "host": "server1.monstermedia.hu",
                            "name": "database_name1",
                            "user": "database_user1",
                            "something":"else",
                         },
                         {
                            "type": "mysql",
                            "host": "server1.monstermedia.hu",
                            "name": "database_name2",
                            "user": "database_user2",
                         },                         
                       ],
                       language: "hu",
                       email: "email@email.hu",
                }, 
             function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, {"url":"https://some.public.facing.tld/installatron/index.php?s=session_id_that_was_just_received"})
                done()

             })

        })

        it('as admin', function(done) {

            app.sendRequestToInstallatron = function(payload) {

                // console.log(payload)

                assert.deepEqual(payload, { user: 'root', key: 'api_key', cmd: 'guixfer' })


                return Promise.resolve({result: JSON.stringify({
                    session_id: "root_session_id_that_was_just_received"
                })})
            }

             mapi.post( "/guixfer", {}, function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, {"url":"https://some.public.facing.tld/installatron/index.php?s=root_session_id_that_was_just_received"})
                done()

             })

        })

	})


    describe("misc features", function(){

        it('deleting a user', function(done) {

             const final_result = {"some":"thing"}

            app.sendRequestToInstallatron = function(payload) {

                 // console.log(payload)

                assert.deepEqual(payload, { user: 'server1-12356',
  key: 'api_key',
  cmd: 'deleteuser'} )


                return Promise.resolve({result: JSON.stringify(final_result)})
            }

             mapi.delete( "/"+username, {}, function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, final_result)
                done()

             })

        })


        it('raw command', function(done) {

             const final_result = {"some":"thing"}

            app.sendRequestToInstallatron = function(payload) {

                 // console.log(payload)

                assert.deepEqual(payload, {key: 'api_key',"hello":"there"})


                return Promise.resolve({result: JSON.stringify(final_result)})
            }

             mapi.post( "/raw", {"hello":"there"}, function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, final_result)
                done()

             })

        })



        it('using the proxy mode', function(done) {

            app.config.set("installatron_proxy_mode", true);
            app.config.set("installatron_ftp_hostport", "foobar.monstermedia.hu:2312");

            app.sendRequestToInstallatron = function(payload) {

                assert.deepEqual(payload, {language: 'hu',
                  server: 'server1.monstermedia.hu',
                  mysql_host: 'server1.monstermedia.hu',
  websites:
   [ { id: 'server1-12356',
       label: 'Some fancy webhosting package',
       path: 'ftp://ftpuser@server1.monstermedia.hu@foobar.monstermedia.hu:2312',
       db_mode: 'manual',
       vhosts: { 'http://some.website1.tld/': '/some/subdirectory1/pages/',
  'http://some.website2.tld/': '/some/subdirectory2/pages/' },
  databases:[] } ],
  email: 'email@email.hu',
  key: 'api_key',
  cmd: 'guixfer' })

                return Promise.resolve({result: JSON.stringify({
                    session_id: "session_id_that_was_just_received"
                })})
            }

             mapi.post( 
                "/webhosting/guixfer",
                {
                       server: "server1",
                       webhosting_id: 12356,
                       ftp_username: "ftpuser",
                       label: "Some fancy webhosting package",
                       "something":"else",
                       vhosts: {
                           "http://some.website1.tld/": "/some/subdirectory1/pages",
                           "http://some.website2.tld/": "/some/subdirectory2/pages",
                       },
                       language: "hu",
                       email: "email@email.hu",
                }, 
             function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, {"url":"https://some.public.facing.tld/installatron/index.php?s=session_id_that_was_just_received"})
                done()

             })

        })

    })



}, 10000)



//permissions needed: ["EMIT_LOG"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "installatron"

    var fs = require("MonsterDotq").fs();

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');

    config.defaults({
      "supported_languages": ["en","hu"],
      "default_language": "en",
      "installatron_proxy_mode": false,
    })

    Array("installatron_server", "installatron_public_url", "installatron_key", "service_primary_domain").forEach(k=>{
      if(!config.get(k)) throw new Error("Mandatory configuration option is missing: "+k)
    })


    const installatron_server = config.get("installatron_server")
    installatron_server.auth_token = "x";
    installatron_server.raw = true;

    var service_primary_domain = config.get("service_primary_domain");

    const clientLib = require("MonsterClient")
    var mapi = clientLib(installatron_server)


    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()



    config.appRestPrefix = "/installatron"



    var app = me.Express(config, moduleOptions);
    app.MError = me.Error

    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)


    var router = app.PromiseRouter(config.appRestPrefix)


    router.post("/webhosting/guixfer", function(req,res,next){

      var d
       return vali.async(req.body.json, {
           webhosting_id: {presence:true, isInteger: {lazy:true}},
           server: {presence:true, isString: {strictName: true}},
           ftp_username: {presence:true, isString: true},
           label: {presence: true, isString: true},
           vhosts: {presence: true, isSimpleObject: true},
           databases: {isArray: true},
           language: {inclusion: app.config.get("supported_languages"), default: "en"},
           email: {presence: true, email: true},
       })
       .then(ad=>{
          d = ad
          d.user = getInstallatronUsername(d.server, d.webhosting_id);
          d.path = getInstallatronPath(d.server, d.ftp_username);

          if(!d.databases) d.databases = []
          return validateVhosts(d.vhosts)
       })
       .then(()=>{
          return validateDatabases(d.databases)
       })
       .then(()=>{

           var server_full_hostname = d.server+"."+service_primary_domain;
           var pl = {
              "language": d.language,
              "server": server_full_hostname,
              "mysql_host": server_full_hostname,
              "websites": [{
                 "id": d.user,
                 "label": d.label,
                 "path": d.path,
                 "db_mode": "manual",
                 "databases": d.databases,
                 "vhosts": d.vhosts
              }],
              "email": d.email,
           }

           return sendInstallatronGuixfer(pl)

       })
       .then(urlResponse=>{
          app.InsertEvent(req, {e_event_type: "guixfer", e_other:{webhosting_id: d.user}});
          return req.sendResponse(urlResponse)
       })


    })
    router.delete("/webhosting/user", function(req,res,next){

        return req.sendPromResultAsIs(
              vali.async(req.body.json, {
                 webhosting_id: {presence:true, isInteger: {lazy:true}},
                 server: {presence:true, isString: {strictName: true}},
              })
              .then(d=>{
                  var user = getInstallatronUsername(d.server, d.webhosting_id);
                  return sendInstallatronCommand({user: user}, "deleteuser")
              })
        )
    })

    router.delete("/:username", function(req,res,next){

        return req.sendPromResultAsIs(
             sendInstallatronCommand({user: req.params.username}, "deleteuser")
        )

    })
    router.post("/guixfer", function(req,res,next){

        return req.sendPromResultAsIs(
             sendInstallatronGuixfer({"user": "root"})
        )

    })
    router.post("/raw", function(req,res,next){

        return req.sendPromResultAsIs(
             sendToInstallatron(req.body.json)
        )

    })
    app.sendRequestToInstallatron=function(payload){
        safePayload = simpleCloneObject(payload)
        safePayload.key = "***"
        const util = require('util');
        console.log("sending request to installatron", util.inspect(safePayload, false, null));
        return mapi.postAsync("/", payload)
    }


    return app

    function sendToInstallatron(raw) {
        if(!raw.key) raw.key = app.config.get("installatron_key")
        return app.sendRequestToInstallatron(raw)
          .then(response=>{
              var re = JSON.parse(response.result);
              console.log("Installatron response", re);
              return re;
          })

    }
    function sendInstallatronCommand(base, command) {
        if(!base.cmd) base.cmd = command

          return sendToInstallatron(base)

    }
    function sendInstallatronGuixfer(base) {
       return sendInstallatronCommand(base, "guixfer")
         .then(response=>{
             var baseurl = app.config.get("installatron_public_url")
             return Promise.resolve({url:baseurl+"/index.php?s="+response.session_id})
         })
    }

    function validateVhosts (vhosts) {
        var ps = []
        Object.keys(vhosts).forEach(host=>{
            var path = vhosts[host]
            // console.log("fooo", host, "x", path)
            ps.push( vali.async({host: host, path: path}, {
                host: {presence: true, isSimpleWebUrl: true},
                path: {presence: true, isPath: true},
            })
            .then(d=>{
               vhosts[d.host]= d.path
            })
            )
        })

        return Promise.all(ps)
      }
    function validateDatabases (dbs) {
        var ps = []

        Object.keys(dbs).forEach(key=>{
            // console.log("array", key, "x", dbs[key])

            ps.push( vali.async(dbs[key], {
                "type": {presence:true, inclusion: ["mysql"]},
                "host": {presence:true, isHost: true},
                "name": {presence: true, isString: true},
                "user": {presence: true, isString: true},
            })
            .then(resolved=>{
                resolved.host = resolved.host.d_host_canonical_name
                dbs[key] = resolved
            }))

        })


        return Promise.all(ps)
      }

  function getInstallatronUsername(server, wh_id){
    return `${server}-${wh_id}`
  }

  function getInstallatronPath(server, ftp_username) {
        var proxy_mode = config.get("installatron_proxy_mode");
        var installatron_ftp_hostport = config.get("installatron_ftp_hostport");

        var hostport = installatron_ftp_hostport ? installatron_ftp_hostport : `${server}.${service_primary_domain}`;
        var target_hostname = server+"."+service_primary_domain;
        var path;
        if(proxy_mode) 
           path = `ftp://${ftp_username}@${target_hostname}@${hostport}`;
        else
           path = `ftp://${ftp_username}@${hostport}`;

        return path;
  }

}

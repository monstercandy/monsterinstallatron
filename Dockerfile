FROM monstercommon

ADD lib /opt/MonsterInstallatron/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterInstallatron/lib/bin/installatron-install.sh && /opt/MonsterInstallatron/lib/bin/installatron-test.sh

ENTRYPOINT ["/opt/MonsterInstallatron/lib/bin/installatron-start.sh"]
